ACCESS_KEY=''
SECRET=''
BUCKET_NAME='' #note that you need to create this bucket first if it doesn't exist

import boto
import boto.s3.connection
from boto.s3.key import Key

def save_file_in_s3(filename):
    conn = boto.connect_s3(aws_access_key_id = ACCESS_KEY,
            aws_secret_access_key = SECRET,
            host = 's3-ap-southeast-1.amazonaws.com')
    bucket = conn.get_bucket(BUCKET_NAME)
    k = Key(bucket)
    k.key = filename
    k.set_contents_from_filename(filename)

def get_file_from_s3(filename):
    conn = boto.connect_s3(aws_access_key_id = ACCESS_KEY,
            aws_secret_access_key = SECRET,
            host = 's3-ap-southeast-1.amazonaws.com')
    bucket = conn.get_bucket(BUCKET_NAME)
    k = Key(bucket)
    k.key = filename
    k.get_contents_to_filename(filename)

def list_files_in_s3():
    conn = boto.connect_s3(aws_access_key_id = ACCESS_KEY,
            aws_secret_access_key = SECRET,
            host = 's3-ap-southeast-1.amazonaws.com')
    bucket = conn.get_bucket(BUCKET_NAME)
    for i, key in enumerate(bucket.get_all_keys()):
        print "[%s] %s" % (i, key.name)

def delete_all_files():
    #FIXME: validate filename exists
    conn = boto.connect_s3(aws_access_key_id = ACCESS_KEY,
            aws_secret_access_key = SECRET,
            host = 's3-ap-southeast-1.amazonaws.com')
    bucket = conn.get_bucket(BUCKET_NAME)
    for i, key in enumerate(bucket.get_all_keys()):
        print "deleting %s" % (key.name)
        key.delete()

def delete_file_from_s3(filename):
    conn = boto.connect_s3(aws_access_key_id = ACCESS_KEY,
            aws_secret_access_key = SECRET,
            host = 's3-ap-southeast-1.amazonaws.com')
    bucket = conn.get_bucket(BUCKET_NAME)
    for i, key in enumerate(bucket.get_all_keys()):
        if key.name == filename:
            key.delete()
            break

if __name__ == '__main__':
    import sys
    if len(sys.argv) < 2:
        print 'Usage: %s <get/set/list/purgeall/delete> <filename>' % (sys.argv[0])
    else:
        if sys.argv[1] == 'set':
            save_file_in_s3(sys.argv[2])
        elif sys.argv[1] == 'get':
            get_file_from_s3(sys.argv[2])
        elif sys.argv[1] == 'list':
            list_files_in_s3()
        elif sys.argv[1] == 'purgeall':
            delete_all_files()
        elif sys.argv[1] == 'delete':
            delete_file_from_s3(sys.argv[2])
        else:
            print 'Usage: %s <get/set/list/purgeall/delete> <filename>' % (sys.argv[0])
