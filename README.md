
# s3box - a simple toolbox to share files via Amazon S3

## Prep
1. sudo apt-get install python-pip (or if you are on Mac, sudo easy_install pip)
2. Install boto package for python. 'sudo pip install boto' or Follow instructions from here: https://github.com/boto/boto
3. Set up AWS S3 credentials and bucket name in 's3box.py'

## Usage
### Upload a file
    python s3box.py set <filename>

### Download a file
    python s3box.py get <filename>

### List files
    python s3box.py list

### Delete all files
    python s3box.py purgeall

### Delete a specific file
    python s3box.py delete <filename>

## Creds
Thanks to Sunil Arora @\_sunil\_ for his [blog post](http://sunilarora.org/database-backup-from-mongodb-to-amazon-s3-and)

